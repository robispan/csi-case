dna_file = open('dna.txt', 'r+')
dna = dna_file.readline()
dna_file.close()

# instead of first three lines we could enter DNA with input:
# dna = input("Please enter DNA sequence found in the crime scene:")

print("Welcome to DNA matcher.\n")
print("DNA sequence entered:")
print(dna + "\n")

is_black = dna.find("CCAGCAATCGC")
is_brown = dna.find("GCCAGTGCCG")
is_blond = dna.find("TTAGCTATCGC")
if is_black != -1:
    hair_color = "Black"
elif is_brown != -1:
    hair_color = "Brown"
elif is_blond != -1:
    hair_color = "Blond"

is_square = dna.find("GCCACGG")
is_round = dna.find("ACCACAA")
is_oval = dna.find("AGGCCTCA")
if is_square != -1:
    facial_shape = "Square"
elif is_round != -1:
    facial_shape = "Round"
elif is_oval != -1:
    facial_shape = "Oval"

is_blue = dna.find("TTGTGGTGGC")
is_green = dna.find("GGGAGGTGGC")
is_brown = dna.find("AAGTAGTGAC")
if is_blue != -1:
    eye_color = "Blue"
elif is_green != -1:
    eye_color = "Green"
elif is_brown != -1:
    eye_color = "Brown"

is_female = dna.find("TGAAGGACCTTC")
is_male = dna.find("TGCAGGAACTTC")
if is_female != -1:
    sex = "Female"
elif is_male != -1:
    sex = "Male"

is_white = dna.find("AAAACCTCA")
is_black = dna.find("CGACTACAG")
is_asian = dna.find("CGCGGGCCG")
if is_white != -1:
    race = "White"
elif is_black != -1:
    race = "Black"
elif is_asian != -1:
    race = "Asian"

print("DNA characteristics: ")
print("Hair color: %s" % hair_color)
print("Facial shape: %s" % facial_shape)
print("Eye color: %s" % eye_color)
print("Sex: %s" % sex)
print("Race: %s \n" % race)

suspect = {
    'name': 'suspect',
    'hair color': hair_color,
    'facial shape': facial_shape,
    'eye color': eye_color,
    'sex': sex,
    'race': race
}

eva = {
    'name': 'Eva',
    'hair color': 'Blonde',
    'facial shape': 'Oval',
    'eye color': 'Blue',
    'sex': 'Female',
    'race': 'White'
}

larisa = {
    'name': 'Larisa',
    'hair color': 'Brown',
    'facial shape': 'Oval',
    'eye color': 'Brown',
    'sex': 'Female',
    'race': 'White'
}

matej = {
    'name': 'Matej',
    'hair color': 'Black',
    'facial shape': 'Oval',
    'eye color': 'Blue',
    'sex': 'Male',
    'race': 'White'
}

miha = {
    'name': 'Miha',
    'hair color': 'Brown',
    'facial shape': 'Square',
    'eye color': 'Green',
    'sex': 'Male',
    'race': 'White'
}
suspects = [eva,larisa,matej,miha]

for x in suspects:
    if x['hair color'] == suspect['hair color'] and x['facial shape'] == suspect['facial shape'] and x['eye color'] == suspect['eye color'] and x['sex'] == suspect['sex'] and x['race'] == suspect['race']:
        guilty = x['name']

print("The person who ate the icecream is %s." % guilty)

input('')